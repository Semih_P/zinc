#include "Application.h"

Application::Application()
{
	m_window = nullptr;
	m_width = m_height = 0;
}
Application::~Application()
{

}

bool Application::Create(void* window, int32 width, int32 height)
{
	m_window = window;
	m_width = width;
	m_height = height;

	m_renderContext = Zinc::RenderContext::Create(window, width, height);


	m_triangle = Zinc::RenderTechniqueTriangle::Create("assets/first_shader.txt");
	return true;
}

void Application::Destroy()
{
	m_renderContext->Destroy();
}

void Application::Update()
{
	//Clear previous frame
	m_renderContext->Clear();

	//Draw
	m_triangle->Draw();

	//Present drawn image
	m_renderContext->Present();
}