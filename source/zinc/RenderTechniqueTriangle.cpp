#include <d3d11.h>
#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

#define SAFE_RELEASE(x) if(x!= nullptr) {x->Release(); x = nullptr;}

#include "zinc\RenderContext.h"
#include "zinc\ServiceLocator.h"
#include <zinc\RenderTechniqueTriangle.h>
#include <cassert>

namespace Zinc
{

	RenderTechniqueTriangle::Ptr RenderTechniqueTriangle::Create(const char* filename)
	{
		FILE* fin = fopen(filename, "r");
		assert(fin && "could not open shader source file");
		fseek(fin, 0, SEEK_END);
		uint32 size = (uint32)ftell(fin);
		fseek(fin, 0, SEEK_SET);

		char source[4096] = { 0 };
		assert(size < sizeof(source) && "shader source to big for buffer");
		uint32 len = (uint32)fread(source, sizeof(char), size, fin);
		fclose(fin);

		source[len] = '\0';

		char* vertexSource = strstr(source, "[vertex]");
		assert(vertexSource);
		*vertexSource = '\0';
		vertexSource += 8;

		char* pixelSource = strstr(vertexSource, "[pixel]");
		assert(pixelSource);
		*pixelSource = '\0';
		pixelSource += 7;
		return Ptr(new RenderTechniqueTriangle(vertexSource, pixelSource));
	}

	RenderTechniqueTriangle::RenderTechniqueTriangle(const char* vertexShaderSource, const char* pixelShaderSource)
	{
		RenderContext* renderContext = ServiceLocator<RenderContext>::GetService();
		ID3D11Device* device = renderContext->GetDevice();

		ID3D11VertexShader* vertexShader = nullptr;
		ID3D11PixelShader* pixelShader = nullptr;
		ID3DBlob* source = nullptr, *error = nullptr;
		ID3DBlob* layout = nullptr;



		///CREATE VERTEX SHADER ////////////////////////////////////////////////////////////////////////////////////////
		HRESULT hr = D3DCompile(vertexShaderSource, strlen(vertexShaderSource), NULL, NULL, NULL, "main", "vs_5_0", 0, 0, &source, &error);
		if (hr != S_OK)
		{
			const char* errorMessage = (const char*)error->GetBufferPointer();
			OutputDebugStringA(errorMessage);
			assert(false && "error in vertex shader source");
		}

		hr = device->CreateVertexShader(
			source->GetBufferPointer(),
			source->GetBufferSize(),
			NULL,
			&vertexShader);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex shader");
		}
		D3DGetInputAndOutputSignatureBlob(source->GetBufferPointer(), source->GetBufferSize(), &layout);

		SAFE_RELEASE(source);
		SAFE_RELEASE(error);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////








		///CREATE PIXEL SHADER ////////////////////////////////////////////////////////////////////////////////////////
		hr = D3DCompile(pixelShaderSource, strlen(pixelShaderSource), NULL, NULL, NULL, "main", "ps_5_0", 0, 0, &source, &error);
		if (hr != S_OK)
		{
			const char* errorMessage = (const char*)error->GetBufferPointer();
			OutputDebugStringA(errorMessage);
			assert(false && "could not create pixel shader");
		}

		hr = device->CreatePixelShader(source->GetBufferPointer(), source->GetBufferSize(), NULL, &pixelShader);
		if (hr != S_OK)
		{
			assert(false && "could not create pixel shader");
		}
		SAFE_RELEASE(source);
		SAFE_RELEASE(error);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////




		/////Create Input layout////////////////////////////////////////////////////////////////////////////////////////
		D3D11_INPUT_ELEMENT_DESC desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, sizeof(float32) * 3, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		ID3D11InputLayout* inputLayout = nullptr;
		hr = device->CreateInputLayout(desc, sizeof(desc) / sizeof(desc[0]), layout->GetBufferPointer(), layout->GetBufferSize(), &inputLayout);
		if (hr != S_OK)
		{
			assert(false && "could not create input layout");
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		struct
		{
			float32 x, y, z;
			uint32 color;
		}
		vertices[] =
		{
			//A B G R
			{ -1.0f, 1.0f, 0.0f,	0xff0000ff },
			{ 1.0f, -1.0f, 0.0f,	0xffff0000 },
			{ -1.0f, -1.0f, 0.0f,	0xffffffff },

			{ -1.0f, 1.0f, 0.0f,	0xff0000ff },
			{ 1.0f, 1.0f, 0.0f,		0xff00ff00 },
			{ 1.0f, -1.0f, 0.0f,	0xffff0000 },
		};

		D3D11_BUFFER_DESC bufDesc = { 0 };
		bufDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufDesc.ByteWidth = sizeof(vertices);
		bufDesc.CPUAccessFlags = 0;
		bufDesc.MiscFlags = 0;
		bufDesc.StructureByteStride = 0;
		bufDesc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = vertices;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		ID3D11Buffer* vertexBuffer = nullptr;
		hr = device->CreateBuffer(&bufDesc, &data, &vertexBuffer);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex buffer");
		}

		m_vertexShader = vertexShader;
		m_pixelShader = pixelShader;
		m_vertexBuffer = vertexBuffer;
		m_inputLayout = inputLayout;
		m_context = renderContext->GetContext();
	}

	RenderTechniqueTriangle::~RenderTechniqueTriangle()
	{

	}

	void RenderTechniqueTriangle::Draw()
	{
		uint32 stride = sizeof(float32) * 3 + sizeof(uint32);
		uint32 offset = 0;

		m_context->VSSetShader(m_vertexShader, NULL, 0);
		m_context->PSSetShader(m_pixelShader, NULL, 0);
		m_context->IASetInputLayout(m_inputLayout);
		m_context->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);
		m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		m_context->Draw(6, 0);
	}
}