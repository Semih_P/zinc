#include <dxgi.h>
#include <d3d11.h>
#include <cassert>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxguid.lib")

#define SAFE_RELEASE(x) if(x!= nullptr) {x->Release(); x = nullptr;}
#include "zinc\RenderContext.h"
#include "zinc\ServiceLocator.h"

namespace Zinc
{
	RenderContext::Ptr RenderContext::Create(void* window, int32 width, int32 height)
	{
		(void)window;
		(void)width;
		(void)height;

		IDXGISwapChain* swap_chain = nullptr;

		DWORD flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
		#if !NDEBUG
			flags |= D3D11_CREATE_DEVICE_DEBUG;
		#endif

		D3D_FEATURE_LEVEL feature_level_request = D3D_FEATURE_LEVEL_11_0;
		D3D_FEATURE_LEVEL feature_level;

		DXGI_SWAP_CHAIN_DESC scd = { 0 };
		scd.BufferDesc.Width = width;
		scd.BufferDesc.Height = height;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.BufferDesc.RefreshRate.Numerator = 60;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.BufferCount = 1;
		scd.SampleDesc.Count = 1;
		scd.SampleDesc.Quality = 0;
		scd.OutputWindow = (HWND)window;
		scd.Windowed = TRUE;
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		ID3D11Device* device = nullptr;
		ID3D11DeviceContext* context = nullptr;

		(void)feature_level;
		(void)context;
		(void)device;
		(void)feature_level_request;
		(void)swap_chain;


		/////////////Swapchain, device, contex///////////////////////////////////////////////////////
		HRESULT hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			flags,
			&feature_level_request,
			1,
			D3D11_SDK_VERSION,
			&scd,
			&swap_chain,
			&device,
			&feature_level,
			&context);

		if (hr != S_OK)
		{
			assert(false && "could not create swap-chain, device or context");
		}
		////////////////////////////////////////////////////////////////////////////////////////////




		//////////Resource//////////////////////////////////////////////////////////////////////////
		ID3D11Resource* resource = nullptr;
		hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&resource);
		if (hr != S_OK)
		{
			assert(false && "could not get backbuffer");
		}
		////////////////////////////////////////////////////////////////////////////////////////////




		////////RenderTargetView///////////////////////////////////////////////////////////////////
		ID3D11RenderTargetView* rtv = nullptr;
		hr = device->CreateRenderTargetView(resource, NULL, &rtv);
		if (hr != S_OK)
		{
			assert(false && "could not create render target view");
		}
		resource->Release();
		////////////////////////////////////////////////////////////////////////////////////////////

		context->OMSetRenderTargets(1, &rtv, NULL);

		D3D11_VIEWPORT viewport = { 0 };
		viewport.Width = (FLOAT)width;
		viewport.Height = (FLOAT)height;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		context->RSSetViewports(1, &viewport);


		RenderContext* result = new RenderContext;
		result->m_swapChain = swap_chain;
		result->m_device = device;
		result->m_context = context;
		result->m_rtv = rtv;


		

		return Ptr(result);
	}

	RenderContext::RenderContext()
	{
		Zinc::ServiceLocator<RenderContext>::SetService(this);
	}
	RenderContext::~RenderContext()
	{
		ServiceLocator<RenderContext>::SetService(nullptr);

	}

	void RenderContext::Clear()
	{
		static const FLOAT color[4] = { 0.5f, 0.7f, 0.3f, 1.0f };
		m_context->ClearRenderTargetView(m_rtv, color);
	}
	void RenderContext::Present()
	{
		m_swapChain->Present(0, 0);
	}

	ID3D11Device* RenderContext::GetDevice()
	{
		return m_device;
	}
	ID3D11DeviceContext* RenderContext::GetContext()
	{
		return m_context;
	}

	void RenderContext::Destroy()
	{
		SAFE_RELEASE(m_rtv);
		SAFE_RELEASE(m_context);
		SAFE_RELEASE(m_device);
		SAFE_RELEASE(m_swapChain);
	}
}