#ifndef APPLICATION_H_INCLUDED
#define APPLICATION_H_INCLUDED

#include <zinc.h>
#include <zinc/RenderContext.h>
#include <zinc/RenderTechniqueTriangle.h>

class Application
{
	//Cannot be copied
	Application(const Application&);
	Application& operator==(const Application&);
	
public:
	Application();
	~Application();

	bool Create(void* window, int32 width, int32 height);
	void Destroy();
	void Update();

private:
	void* m_window;
	int32 m_width;
	int32 m_height;

	Zinc::RenderContext::Ptr m_renderContext;
	Zinc::RenderTechniqueTriangle::Ptr m_triangle;
};

#endif