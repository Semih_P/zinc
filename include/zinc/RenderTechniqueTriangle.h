#ifndef RENDERTECHNIQUETRIANGLE_H_INCLUDED
#define RENDERTECHNIQUETRIANGLE_H_INCLUDED

#include <Zinc.h>

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11Buffer;
struct ID3D11InputLayout;
struct ID3D11DeviceContext;

namespace Zinc
{
	class RenderTechniqueTriangle
	{
		RenderTechniqueTriangle(const RenderTechniqueTriangle&);
		RenderTechniqueTriangle& operator==(const RenderTechniqueTriangle&);

	public:
		typedef std::unique_ptr<RenderTechniqueTriangle> Ptr;

		static Ptr Create(const char* filename);

	private:
		RenderTechniqueTriangle(const char* vertexShaderSource, const char* pixelShaderSource);

	public:
		~RenderTechniqueTriangle();

		void Draw();

	private:
		ID3D11VertexShader* m_vertexShader;
		ID3D11PixelShader* m_pixelShader;
		ID3D11Buffer* m_vertexBuffer;
		ID3D11InputLayout* m_inputLayout;
		ID3D11DeviceContext* m_context;
	};
}


#endif