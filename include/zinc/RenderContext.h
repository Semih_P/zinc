#ifndef RENDERCONTEXT_H_INCLUDED
#define RENDERCONTEXT_H_INCLUDED

#include <Zinc.h>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;

namespace Zinc
{
	class RenderContext
	{
		RenderContext(const RenderContext&);
		RenderContext& operator==(const RenderContext&);

	public:
		typedef std::unique_ptr<RenderContext> Ptr;
		static Ptr Create(void* window, int32 width, int32 height);

	private:
		RenderContext();

	public:
		~RenderContext();

		void Destroy();
		void Clear();
		void Present();

		ID3D11Device* GetDevice();
		ID3D11DeviceContext* GetContext();

	private:
		IDXGISwapChain* m_swapChain;
		ID3D11Device* m_device;
		ID3D11DeviceContext* m_context;
		ID3D11RenderTargetView* m_rtv;
	};
}

#endif